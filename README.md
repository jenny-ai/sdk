<p align="center">
  <h1 align="center"> Hotel Guest Smart Assistant Box SDK </h1>
</p>

<p align="center">
  <b>An SDK</b> that <i>every ability needs</i>.
</p>

---

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Description](#description)
- [Features](#features)

---

## Description

The SDK that handles most system calls in-and-out of every device.
<br>

## Features
- [x] Communication SDK
- [x] Database Client SDK
- [x] Local Server SDK
- [ ] Speech SDK
- [x] Status SDK

<br>
