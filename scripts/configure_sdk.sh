#!/bin/sh
#
# NAME: CONFIG SDK


echo "Installing required python packages..."

if ! which pip3 > /dev/null; then
    echo -e "pip3 not found! install it? (Y/n) \c"
    read
    if [ "$REPLY" = "n" ]; then
        exit 0
    fi
    sudo apt install python3-pip -y
fi

pip3 install gevent loguru paho-mqtt pymysql tinydb ujson

echo "Installed required python packages succesfully."


echo "Installing jenny SDK..."

if [ ! -d ./sdk ]; then
    echo "error: SDK folder not found, did you run the script from the correct directory?"
    exit 1
fi
if [ ! -d ../jenny ]; then
    echo "error: Assistant folder not found, did you run the script from the correct directory?"
    exit 1
fi

echo "Python Path [/home/pi/.local/lib/python3.6/site-packages]: \c"
read
if [ -z "${REPLY}" ]; then
    py_packages_path=/home/pi/.local/lib/python3.6/site-packages
else
    py_packages_path=${REPLY}
fi

if [ -d "${py_packages_path}/jenny" ]; then
    echo "error: Jenny SDK directory exists."
    echo -e "Do you want to delete it? (Y/n) \c"
    read
    if [ "${REPLY}" = "n" ]; then
        exit 0
    fi
    rm -r "${py_packages_path}/jenny"
fi

mkdir "${py_packages_path}/jenny"
if [ ! -d "${py_packages_path}/jenny" ]; then
    echo "error: Creating Jenny SDK directory failed."
    exit 1
fi

cp -r ./sdk/* "${py_packages_path}/jenny/"
echo "Installed Jenny SDK succesfully."


exit 0

