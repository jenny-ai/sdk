from loguru import logger


class SpeechObject(object):
    def __init__(self, text):

        self.text = str(text)

    def say(self):

        logger.debug("Saying: " + self.text)
