from enum import Enum

import ujson as json
from loguru import logger


class Data(Enum):

    MAPS_URL = "00"
    MQTTCertificateSigningRequest = "01"
    MQTTCertificateAuthorityConfirm = "02"
    MQTTCertificate = "03"

    MPDTogglePlayingStatus = "0a"
    MPDNextOrPreviousSong = "0b"
    MPDSongName = "0c"
    MPDSongTime = "0d"
    MPDSeekToTime = "0e"
    MPDSongPicture = "0f"
    MPDStartMediaPlayer = "0g"
    MPDSetVolume = "0h"

    IOTTVLightIntensity = "10"
    IOTTVSoundIntensity = "11"
    IOTTVToggleStatus = "12"
    IOTLEDLightIntensity = "13"
    IOTLEDToggleStatus = "14"

    HOTELEMERGENCY = "h0"
    HOTELGUESTDATA = "h1"
    HOTELPLACERECOMMENDATION = "h2"
    HOTELRESTURANTRECOMMENDATION = "h3"
    HOTELSOUNDS = "h4"
    HOTELTRANSPORTRECOMMENDATION = "h5"
    HOTELWIFI = "h6"
    HOTELDATA = "h7"

    NULL = "ZZ"


def send_to_android_app(data_id, data):
    """
    :param data_id: Data identification number, used by the android application
    :type data_id: communication.Data enum class
    :param data: String of data to be sent
    :type data: string

    """

    from jenny.status import Assistant
    from jenny.protocol import reactor

    with Assistant() as assistant:

        if not isinstance(data, dict):
            data = {
                "dtype": Data(data_id).value,
                "data": data,
                "source_type": assistant.type,
                "source_id": assistant.id,
                "destination_type": "phone",
                "destination_id": assistant.id,
            }

        reactor.publish(
            topic="phones/{}".format(assistant.id), payload=json.dumps(data), qos=2
        )

    logger.debug(
        "Sent {}:{} to connected android applications.".format(Data(data_id), data)
    )


def send_to_assistant(assistant_id, data_id, data):
    """
    :param assistant_id: Destination assistant ID
    :type assistant_id: int
    :param data_id: Data identification number, used by the remote assistant
    :type data_id: communication.Data enum class
    :param data: String of data to be sent
    :type data: string

    """

    from jenny.status import Assistant
    from jenny.protocol import reactor

    with Assistant() as assistant:

        if not isinstance(data, dict):
            data = {
                "dtype": Data(data_id).value,
                "data": data,
                "source_type": assistant.type,
                "source_id": assistant.id,
                "destination_type": "assistant",
                "destination_id": assistant_id,
            }

    reactor.publish(
        topic="assistants/{}".format(assistant_id), payload=json.dumps(data), qos=2
    )

    logger.debug(
        "Sent {}:{} to assistant {}.".format(Data(data_id), data, assistant_id)
    )


def send_to_hotel(data_id, data):
    """
    :param data_id: Data identification number, used by the remote assistant
    :type data_id: communication.Data enum class
    :param data: String of data to be sent
    :type data: string

    """

    from jenny.status import Assistant
    from jenny.protocol import reactor

    with Assistant() as assistant:

        if not isinstance(data, dict):
            data = {
                "dtype": Data(data_id).value,
                "data": data,
                "source_type": assistant.type,
                "source_id": assistant.id,
                "destination_type": "hotel",
                "destination_id": None,
            }

        if assistant.type == "assistant":
            reactor.publish(topic="local", payload=json.dumps(data), qos=2)
        else:
            reactor.publish(topic="hotel", payload=json.dumps(data), qos=2)

    logger.debug("Sent {}:{} to hotel application.".format(Data(data_id), data))


def send_to_local_server(data_id, data):
    """
    :param data_id: Data identification number, used by the remote assistant
    :type data_id: communication.Data enum class
    :param data: String of data to be sent
    :type data: string

    """

    from jenny.status import Assistant
    from jenny.protocol import reactor

    with Assistant() as assistant:

        if not isinstance(data, dict):
            data = {
                "dtype": Data(data_id).value,
                "data": data,
                "source_type": assistant.type,
                "source_id": assistant.id,
                "destination_type": "local",
                "destination_id": None,
            }

    reactor.publish(topic="local", payload=json.dumps(data), qos=2)

    logger.debug("Sent {}:{} to local server.".format(Data(data_id), data))


__all__ = [
    "Data",
    "send_to_android_app",
    "send_to_assistant",
    "send_to_hotel",
    "send_to_local_server",
]
