import datetime

import pymysql
from loguru import logger

from jenny.status import Assistant


class Client(object):
    def __init__(self):

        logger.debug("Preparing database client...")

        # self.cloud = pymysql.connect(host='remotemysql.com', port=3306, user='r44NaoeIly', passwd='AAzN1mKv29', db='r44NaoeIly', charset='utf8')
        self.local = pymysql.connect(
            host="remotemysql.com",
            port=3306,
            user="AXI5ifAYDM",
            passwd="CngMKA7EoX",
            db="AXI5ifAYDM",
            charset="utf8",
        )
        # self.assistant = pymysql.connect(host='127.0.0.1', port=3306, user='a', passwd='a', db='a', charset='utf8')

    def food_recommendations(self):
        with self.cloud.cursor(pymysql.cursors.DictCursor) as cursor:
            with Assistant(init_hotel=True) as assistant:
                sql = '''SELECT place.id, place.address
                         FROM place, place_recommendation
                         WHERE place.id=place_recommendation.place_id
                         AND place.city="{}"
                         AND place.country="{}"'''.format(
                    assistant.city, assistant.country
                )
            cursor.execute(sql)
            result = cursor.fetchall()

        return result

    def feedback_questions(self):
        with self.cloud.cursor(pymysql.cursors.DictCursor) as cursor:
            sql = """SELECT feedback_question.id, feedback_question.value
                     FROM feedback_question"""
            cursor.execute(sql)
            result = cursor.fetchall()

        return result

    def retrieve_place_address(self, place_id):
        with self.cloud.cursor(pymysql.cursors.DictCursor) as cursor:
            sql = '''SELECT place.id, place.address
                     FROM place
                     WHERE place.id="{}"'''.format(
                place_id
            )
            cursor.execute(sql)
            result = cursor.fetchall()

        return result[0]

    def store_cloud_feedback(self, feedback_question_id, feedback):
        with self.cloud.cursor() as cursor:
            with Assistant() as assistant:
                sql = """INSERT INTO `feedback_answer` (`value`, `feedback_question_id`, `Session_id`)
                         VALUES ("{}", {}, {})""".format(
                    feedback, feedback_question_id, assistant.session_id
                )
            cursor.execute(sql)
        self.cloud.commit()

    def store_local_feedback(self, feedback_question_id, feedback):
        with self.local.cursor() as cursor:
            with Assistant() as assistant:
                sql = """INSERT INTO `feedback_answer` (`value`, `feedback_question_id`, `Session_id`)
                         VALUES ("{}", {}, {})""".format(
                    feedback, feedback_question_id, assistant.session_id
                )
            cursor.execute(sql)
        self.local.commit()

    def add_joke_local(self, joke_id, joke_text):
        with self.local.cursor() as cursor:
            sql = """INSERT INTO `jokes` (`id`, `value`)
                         VALUES ("{}", "{}")""".format(
                joke_id, joke_text
            )
            cursor.execute(sql)
        self.local.commit()

    def delete_jokes_local(self):
        with self.local.cursor() as cursor:
            sql = """Delete FROM `jokes`"""
            cursor.execute(sql)
        self.local.commit()

    def retrieve_joke_local(self, id):
        with self.local.cursor(pymysql.cursors.DictCursor) as cursor:
            sql = '''SELECT jokes.value
                     FROM jokes
                     WHERE jokes.id="{}"'''.format(
                id
            )
            cursor.execute(sql)
        result = cursor.fetchall()
        return result[0]["value"]

    def add_quote_local(self, value):
        with self.local.cursor() as cursor:
            sql = """INSERT INTO `quote` (`content`,`sayer`)
                         VALUES ("{}","{}")""".format(
                value[0], value[1]
            )
            cursor.execute(sql)
        self.local.commit()

    def delete_quote_local(self):
        with self.local.cursor() as cursor:
            sql = """Delete FROM quote"""
            cursor.execute(sql)
        self.local.commit()

    def retrieve_quote(self):
        with self.local.cursor(pymysql.cursors.DictCursor) as cursor:
            sql = """SELECT quote.content,quote.sayer
                     FROM quote
                     """
            cursor.execute(sql)
        result = cursor.fetchall()
        return result[0]["content"], result[0]["sayer"]

    def __del__(self):
        self.cloud.close()
        self.local.close()


__all__ = ["Client"]
