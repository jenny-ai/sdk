import sys

del sys.modules["jenny.protocol.reactor"]

from loguru import logger
from paho.mqtt.client import Client

from jenny.protocol.crypt import assert_crt
from jenny.shared import current_running_script_path
from jenny.status import Assistant


with Assistant() as assistant:
    if assistant.type != "local" and assistant.encrypt_communication:
        assert_crt()


def on_connect(client, userdata, flags, rc):

    logger.debug("Connected to the MQTT broker succesfully.")

    with Assistant() as assistant:

        if assistant.type == "assistant":
            logger.debug("Trying to subscribe to assistants/{}.".format(assistant.id))
            client.subscribe(topic="assistants/{}".format(assistant.id), qos=2)
        elif assistant.type == "local":
            logger.debug("Trying to subscribe to /local.")
            client.subscribe(topic="local", qos=2)


@logger.catch
def on_message(client, userdata, msg):

    msg = msg.payload.decode()

    from jenny.communication import Data

    logger.debug("Recieved {}.".format(msg))

    from jenny.protocol.handler import Handler

    Handler(msg)


def on_publish(client, userdata, mid):

    logger.debug("Message published succesfully.")


def on_subscribe(client, userdata, mid, granted_qos):

    logger.debug("Subscribed with qos={} succesfully.".format(granted_qos[0]))


with Assistant() as assistant:
    global reactor

    if assistant.type == "assistant":
        reactor = Client(
            client_id="assistant{}".format(assistant.id), clean_session=False
        )
    elif assistant.type == "local":
        reactor = Client(client_id="local", clean_session=False)

    reactor.on_connect = on_connect
    reactor.on_message = on_message
    reactor.on_publish = on_publish
    reactor.on_subscribe = on_subscribe

    if assistant.encrypt_communication:
        current_script_path = current_running_script_path(__file__)
        reactor.tls_set(
            ca_certs=current_script_path + "/certs/ca.crt",
            certfile=current_script_path + "/certs/client.crt",
            keyfile=current_script_path + "/certs/client.key",
            tls_version=2,
        )
        reactor.tls_insecure_set(
            True
        )  # Turn on if the host name in the connect paramater doesn't match the server's common key name.
        reactor.connect(host="jennylocal", port=8883, keepalive=60)
    else:
        reactor.connect(host="jennylocal", port=1883, keepalive=60)

reactor.loop_start()
sys.modules["jenny.protocol.reactor"] = reactor

__all__ = ["reactor"]
