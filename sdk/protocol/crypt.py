import datetime

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509 import load_pem_x509_certificate
from cryptography.x509.oid import NameOID
from gevent import sleep

from jenny.protocol.networking import broadcast, start_server
from jenny.shared import current_running_script_path
from jenny.status import Assistant

# Client Funcions


def generate_private_key(
    path=current_running_script_path(__file__) + "/certs/",
    file_name="client.key",
    bits=2048,
):

    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=bits, backend=default_backend()
    )

    with open(path + file_name, "wb") as dst:
        dst.write(
            private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            )
        )

    return private_key


def load_private_key(
    path=current_running_script_path(__file__) + "/certs/", file_name="client.key"
):

    with open(path + file_name, "rb") as src:
        private_key = serialization.load_pem_private_key(
            src.read(), password=None, backend=default_backend()
        )

    return private_key


def generate_csr(
    path=current_running_script_path(__file__) + "/certs/", file_name="client.csr"
):

    try:
        private_key = load_private_key(path=path)
    except:
        private_key = generate_private_key(path=path)

    with Assistant() as assistant:
        request = (
            x509.CertificateSigningRequestBuilder()
            .subject_name(
                x509.Name(
                    [
                        x509.NameAttribute(
                            NameOID.COMMON_NAME, "assistant" + str(assistant.id)
                        )
                    ]
                )
            )
            .add_extension(
                x509.BasicConstraints(ca=False, path_length=None), critical=True
            )
            .sign(private_key, hashes.SHA256(), default_backend())
            .public_bytes(encoding=serialization.Encoding.PEM)
        )

    with open(path + file_name, "wb") as dst:
        dst.write(request)

    return request


def request_crt():

    from jenny.communication import Data

    with Assistant() as assistant:
        assistant.set_requesting_crt(True)
        assistant.sync()

    start_server()
    broadcast(Data.MQTTCertificateSigningRequest)


def write_certificate_from_buffer(
    buffer,
    path=current_running_script_path(__file__) + "/certs/",
    file_name="client.crt",
):

    with open(path + file_name, "wb") as dst:
        dst.write(buffer.encode())


def load_crt(
    path=current_running_script_path(__file__) + "/certs/", file_name="client.crt"
):

    with open(path + file_name, "rb") as src:
        crt = x509.load_pem_x509_certificate(src.read(), backend=default_backend())

    return crt


def assert_crt(path=current_running_script_path(__file__) + "/certs/"):

    try:
        load_crt()
    except:
        request_crt()
        sleep(0.5)
        assert_crt()


# Local Server Functions


def sign_csr_from_buffer(buffer):

    csr = load_csr_from_buffer(buffer.encode())
    ca_crt = load_crt(path="/etc/mosquitto/ca_certificates/", file_name="ca.crt")
    ca_private_key = load_private_key(
        path="/etc/mosquitto/ca_certificates/", file_name="ca.key"
    )

    crt = (
        x509.CertificateBuilder()
        .subject_name(csr.subject)
        .issuer_name(ca_crt.subject)
        .public_key(csr.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(datetime.datetime.utcnow())
        .not_valid_after(datetime.datetime.utcnow() + datetime.timedelta(days=3650))
        .add_extension(
            extension=x509.KeyUsage(
                digital_signature=True,
                key_encipherment=True,
                content_commitment=True,
                data_encipherment=False,
                key_agreement=False,
                encipher_only=False,
                decipher_only=False,
                key_cert_sign=False,
                crl_sign=False,
            ),
            critical=True,
        )
        .add_extension(
            extension=x509.BasicConstraints(ca=False, path_length=None), critical=True
        )
        .add_extension(
            extension=x509.AuthorityKeyIdentifier.from_issuer_public_key(
                ca_private_key.public_key()
            ),
            critical=False,
        )
        .sign(
            private_key=ca_private_key,
            algorithm=hashes.SHA256(),
            backend=default_backend(),
        )
        .public_bytes(encoding=serialization.Encoding.PEM)
    )

    return crt


def load_csr_from_buffer(buffer):

    return x509.load_pem_x509_csr(buffer, backend=default_backend())
