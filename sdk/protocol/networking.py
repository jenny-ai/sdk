from gevent import socket
from gevent.server import DatagramServer
from loguru import logger

from jenny.shared import local_lan_address

udp_port = 3936


class Server(DatagramServer):

    server = None

    def handle(self, data, address):

        if address[0] != local_lan_address():

            logger.debug("Recieved {} from {}.".format(data, address[0]))

            from jenny.protocol.handler import LowLevelHandler

            LowLevelHandler(data, address[0])


def start_server(port=None):

    if Server.server == None:
        if port == None:
            port = udp_port

        server = Server(":" + str(port))
        server.start()
        Server.server = server

        logger.success("Started datagram server at :{}.".format(port))
    else:
        server = Server.server
        logger.success("Datagram server already running.")

    return server


def broadcast(message, port=None):

    if port == None:
        port = udp_port

    address = ("255.255.255.255", port)
    sock = socket.socket(type=socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.connect(address)
    sock.send(message.value.encode())
    sock.close()

    logger.success("Broadcasted {} at :{}.".format(message, port))


def send_message(message, address, port=None):

    if port == None:
        port = udp_port

    address = (address, port)

    sock = socket.socket(type=socket.SOCK_DGRAM)
    sock.connect(address)
    sock.send(message)
    sock.close()

    logger.success("Sent {} to {}.".format(message, address))


__all__ = ["start_server", "broadcast", "send_message"]
