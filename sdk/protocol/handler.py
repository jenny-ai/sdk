import ujson as json
from loguru import logger
from tinydb import Query, TinyDB
from tinydb.middlewares import CachingMiddleware
from tinydb.storages import JSONStorage

from jenny.client import Client
from jenny.communication import (
    Data,
    send_to_android_app,
    send_to_assistant,
    send_to_hotel,
    send_to_local_server,
)
from jenny.protocol.networking import send_message
from jenny.shared import current_running_script_path
from jenny.status import Assistant, Guest, Hotel


class Handler(object):
    def __init__(self, msg):

        msg = json.loads(msg)

        self.msg_type, self.msg_content = Data(msg["dtype"]), msg["data"]
        self.msg_src_type, self.msg_src_id = msg["source_type"], msg["source_id"]
        self.msg_dst_type, self.msg_dst_id = (
            msg["destination_type"],
            msg["destination_id"],
        )

        with Assistant() as assistant:

            if self.msg_dst_type == "assistant" and assistant.type == "local":
                send_to_assistant(self.msg_dst_id, Data.NULL, msg)

            elif self.msg_dst_type == "hotel" and assistant.type == "local":
                send_to_hotel(Data.NULL, msg)

            elif self.msg_dst_type == "local" and assistant.type == "local":

                if self.msg_type == Data.HOTELDATA:
                    from jenny.status import Hotel

                    with Hotel() as hotel:
                        hotel._db.purge()
                        hotel._db.insert(
                            {
                                **{
                                    "dname": "data",
                                    "init": True,
                                    "requesting_hotel_data": False,
                                },
                                **self.msg_content[0],
                            }
                        )
                        hotel._db.close()

            elif self.msg_dst_type == "assistant" and assistant.type == "assistant":

                if self.msg_type == Data.HOTELDATA:
                    from jenny.status import Hotel

                    with Hotel() as hotel:
                        hotel._db.purge()
                        hotel._db.insert(
                            {
                                **{
                                    "dname": "data",
                                    "init": True,
                                    "requesting_hotel_data": False,
                                },
                                **self.msg_content[0],
                            }
                        )
                        hotel._db.close()

                elif self.msg_type == Data.HOTELGUESTDATA:
                    from jenny.assistant.send_mail import send

                    guest_data = self.msg_content[0]
                    with Guest() as guest:
                        guest.set_name(guest_data["name"])
                        guest.set_email(guest_data["mail"])
                        guest.sync()

                    send(guest_data["name"].split(" ")[0], guest_data["mail"])

                elif self.msg_type == Data.HOTELWIFI:
                    from jenny.status import Requester

                    with Requester() as requester:
                        if requester.requesting_wifi_password:
                            requester.set_wifi_password(self.msg_content)
                            requester.set_requesting_wifi_password(False)
                            requester.sync()

                elif self.msg_type == Data.HOTELEMERGENCY:
                    from jenny.status import Requester

                    with Requester() as requester:
                        if requester.requesting_emergency:
                            requester.set_requesting_emergency(False)
                            requester.sync()

                elif self.msg_type == Data.HOTELSOUNDS:
                    from jenny.status import Requester

                    with Requester() as requester:
                        if requester.requesting_hotel_sounds_playlist:
                            requester.set_hotel_sounds_playlist(self.msg_content)
                            requester.set_requesting_hotel_sounds_playlist(False)
                            requester.sync()

                elif self.msg_type in (
                    Data.MPDTogglePlayingStatus,
                    Data.MPDNextOrPreviousSong,
                    Data.MPDSeekToTime,
                    Data.MPDSetVolume,
                ):

                    self.music_player_handler()

        logger.debug(
            "Handled {}:{} succesfully.".format(self.msg_type, self.msg_content)
        )

    def music_player_handler(self):

        import jenny.assistant.mopidy_actions as mopidy

        if self.msg_type == Data.MPDTogglePlayingStatus:

            if mopidy.state() == "play":
                mopidy.pause()
            elif mopidy.state() == "pause":
                mopidy.resume()

        elif self.msg_type == Data.MPDNextOrPreviousSong:

            if self.msg_content == "n":
                mopidy.next()
            elif self.msg_content == "p":
                mopidy.previous()

        elif self.msg_type == Data.MPDSeekToTime:

            mopidy.jump_to(int(self.msg_content))

        elif self.msg_type == Data.MPDSetVolume:

            mopidy.set_volume(int(self.msg_content))


class LowLevelHandler(object):
    def __init__(self, msg, addr):

        msg = msg.decode()
        self.msg_type, self.msg_content = Data(msg[:2]), msg[2:]

        with Assistant() as assistant:

            if (
                self.msg_type == Data.MQTTCertificateSigningRequest
                and assistant.type == "local"
            ):

                if len(self.msg_content) == 0:
                    send_message(
                        Data.MQTTCertificateAuthorityConfirm.value.encode(), addr
                    )
                else:

                    from jenny.protocol.crypt import sign_csr_from_buffer

                    crt = sign_csr_from_buffer(self.msg_content)
                    send_message(Data.MQTTCertificate.value.encode() + crt, addr)

            elif (
                self.msg_type == Data.MQTTCertificateAuthorityConfirm
                and assistant.requesting_crt
            ):

                from jenny.protocol.crypt import generate_csr

                send_message(
                    Data.MQTTCertificateSigningRequest.value.encode() + generate_csr(),
                    addr,
                )

            elif self.msg_type == Data.MQTTCertificate and assistant.requesting_crt:

                from jenny.protocol.crypt import write_certificate_from_buffer

                write_certificate_from_buffer(self.msg_content)
                assistant.set_requesting_crt(False)
                assistant.sync()

        logger.debug(
            "Handled {}:{} from {}.".format(self.msg_type, self.msg_content, addr)
        )


__all__ = ["Handler", "LowLevelHandler"]
