import os
import socket


def current_running_script_path(file):

    return os.path.dirname(os.path.abspath(file))


def local_lan_address():

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 1))
        ip = s.getsockname()[0]
    except:
        ip = "127.0.0.1"
    finally:
        s.close()

    return ip
