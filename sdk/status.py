from gevent import sleep
from loguru import logger
from tinydb import Query, TinyDB
from tinydb.middlewares import CachingMiddleware
from tinydb.storages import JSONStorage

from jenny.communication import Data, send_to_hotel
from jenny.shared import current_running_script_path, local_lan_address


class Guest(object):

    db_path = current_running_script_path(__file__) + "/db/guest.json"

    def __enter__(self):
        """
        Used to compute, populate or create guest status variables

        """

        self._db = TinyDB(Guest.db_path, storage=CachingMiddleware(JSONStorage))
        q = Query()
        data = self._db.get(q.dname == "data")
        self.name = data["name"]
        self.email = data["email"]

        logger.debug("Loaded guest data {}:{}".format(self.name, self.email))

        return self

    def __exit__(self, type, value, traceback):
        """
        Used to destroy any dynamic data created by __enter__

        """

        pass

    def set_name(self, name):

        self.name = name

    def set_email(self, email):

        self.email = email

    def sync(self):

        self._db.purge()
        self._db.insert({"dname": "data", "name": self.name, "email": self.email})
        self._db.close()

        logger.debug("Synced guest data {}:{}".format(self.name, self.email))


class Setting(object):

    db_path = current_running_script_path(__file__) + "/db/setting.json"

    def __enter__(self):
        """
        Used to compute, populate or create guest status variables

        """

        self._db = TinyDB(Setting.db_path, storage=CachingMiddleware(JSONStorage))
        q = Query()
        data = self._db.get(q.dname == "data")
        self.reminder_default_assistant = data["reminder_default_assistant"]
        self.reminder_default_google_calendar = data["reminder_default_google_calendar"]
        self.alarm_snooze_interval = data["alarm_snooze_interval"]
        self.alarm_n_snooze = data["alarm_n_snooze"]
        self.alarm_ringtone_name = data["alarm_ringtone_name"]
        self.weather_location_country = data["weather_location_country"]
        self.weather_location_city = data["weather_location_city"]
        self.news_location_country = data["news_location_country"]
        self.news_interest_sport = data["news_interest_sport"]
        self.news_interest_business = data["news_interest_business"]
        self.news_interest_entertainment = data["news_interest_entertainment"]
        self.news_interest_health = data["news_interest_health"]
        self.news_interest_science = data["news_interest_science"]
        self.morning_text_qoute = data["morning_text_qoute"]
        self.morning_text_mail = data["morning_text_mail"]
        self.morning_text_news = data["morning_text_news"]
        self.morning_text_weather = data["morning_text_weather"]

        return self

    def __exit__(self, type, value, traceback):
        """
        Used to destroy any dynamic data created by __enter__

        """

        pass


class Assistant(object):

    db_path = current_running_script_path(__file__) + "/db/assistant.json"

    def __init__(self, init_hotel=False, init_network=False):

        self.init_hotel = init_hotel
        self.init_network = init_network

    def __enter__(self):
        """
        Used to compute, populate or create assistant status variables

        """

        if self.init_hotel:
            with Hotel() as hotel:
                self.hotel = hotel.name
                self.country = hotel.country
                self.city = hotel.city
                self.latitude = hotel.latitude
                self.longitude = hotel.longitude
                self.location = hotel.location

        if self.init_network:
            self.ip_address = local_lan_address()

        self._db = TinyDB(Assistant.db_path, storage=CachingMiddleware(JSONStorage))
        q = Query()
        data = self._db.get(q.dname == "data")
        self.type = data["type"]
        self.id = int(data["id"])
        self.room_id = int(data["room_id"])
        self.user_id = int(data["user_id"])
        self.session_id = int(data["session_id"])
        self.groups = data["groups"]
        self.requesting_crt = data["requesting_crt"]
        self.is_connected_to_application = data["is_connected_to_application"]
        self.is_connected_to_uber = data["is_connected_to_uber"]
        self.joke_index = int(data["joke_index"])
        self.encrypt_communication = data["encrypt_communication"]

        return self

    def __exit__(self, type, value, traceback):
        """
        Used to destroy any dynamic data created by __enter__

        """

        pass

    def set_requesting_crt(self, requesting_crt):

        self.requesting_crt = requesting_crt

        logger.debug(
            "Requesting certificate from local server: {}.".format(self.requesting_crt)
        )

    def increment_joke_index(self):

        self.joke_index = (self.joke_index + 1) % 20
        self.sync()

    def sync(self):

        self._db.purge()
        self._db.insert(
            {
                "dname": "data",
                "type": self.type,
                "id": self.id,
                "room_id": self.room_id,
                "user_id": self.user_id,
                "session_id": self.session_id,
                "groups": self.groups,
                "requesting_crt": self.requesting_crt,
                "is_connected_to_application": self.is_connected_to_application,
                "is_connected_to_uber": self.is_connected_to_uber,
                "joke_index": self.joke_index,
                "encrypt_communication": self.encrypt_communication,
            }
        )
        self._db.close()

        logger.debug("Synced assistant data.")


class Hotel(object):

    db_path = current_running_script_path(__file__) + "/db/hotel.json"

    def __enter__(self):
        """
        Used to compute, populate or create hotel status variables

        """

        self._db = TinyDB(Hotel.db_path, storage=CachingMiddleware(JSONStorage))
        q = Query()
        data = self._db.get(q.dname == "data")

        self.init = data["init"]
        self.requesting_hotel_data = data["requesting_hotel_data"]
        self.name = data["name"]
        self.country = data["country"]
        self.city = data["city"]
        self.latitude = float(data["latitude"])
        self.longitude = float(data["longitude"])
        self.location = "{},{}".format(self.latitude, self.longitude)

        if not self.init and not self.requesting_hotel_data:
            self.set_requesting_hotel_data(True)
            self.sync()

            self.wait_for(Data.HOTELDATA)

            self._db = TinyDB(Hotel.db_path, storage=CachingMiddleware(JSONStorage))
            q = Query()
            data = self._db.get(q.dname == "data")
            self.init = data["init"]
            self.requesting_hotel_data = data["requesting_hotel_data"]
            self.name = data["name"]
            self.country = data["country"]
            self.city = data["city"]
            self.latitude = float(data["latitude"])
            self.longitude = float(data["longitude"])
            self.location = "{},{}".format(self.latitude, self.longitude)

        return self

    def __exit__(self, type, value, traceback):
        """
        Used to destroy any dynamic data created by __enter__

        """

        pass

    def set_requesting_hotel_data(self, requesting_hotel_data):

        self.requesting_hotel_data = requesting_hotel_data

        logger.debug(
            "Requesting hotel data from the hotel: {}.".format(
                self.requesting_hotel_data
            )
        )

    def wait_for(self, event):

        waiting = True

        if event is Data.HOTELDATA:
            while waiting:
                send_to_hotel(Data.HOTELDATA, "")
                sleep(0.5)
                with Hotel() as hotel:
                    if hotel.init:
                        waiting = False

    def sync(self):

        self._db.purge()
        self._db.insert(
            {
                "dname": "data",
                "init": self.init,
                "requesting_hotel_data": self.requesting_hotel_data,
                "name": self.name,
                "country": self.country,
                "city": self.city,
                "latitude": self.latitude,
                "longitude": self.longitude,
            }
        )
        self._db.close()

        logger.debug("Synced hotel data.")


class Requester(object):

    db_path = current_running_script_path(__file__) + "/db/requester.json"

    def __enter__(self):
        """
        Used to compute, populate or create assistant status variables

        """

        self._db = TinyDB(Requester.db_path, storage=CachingMiddleware(JSONStorage))
        q = Query()
        data = self._db.get(q.dname == "data")
        self.requesting_wifi_password = data["requesting_wifi_password"]
        self.wifi_password = data["wifi_password"]
        self.requesting_hotel_sounds_playlist = data["requesting_hotel_sounds_playlist"]
        self.hotel_sounds_playlist = data["hotel_sounds_playlist"]
        self.requesting_emergency = data["requesting_emergency"]

        return self

    def __exit__(self, type, value, traceback):
        """
        Used to destroy any dynamic data created by __enter__

        """

        pass

    def set_requesting_wifi_password(self, requesting_wifi_password):

        self.requesting_wifi_password = requesting_wifi_password

        logger.debug(
            "Requesting wifi password from hotel: {}.".format(
                self.requesting_wifi_password
            )
        )

    def set_wifi_password(self, wifi_password):

        self.wifi_password = wifi_password

        logger.debug("Setting wifi password: {}.".format(self.wifi_password))

    def set_requesting_hotel_sounds_playlist(self, requesting_hotel_sounds_playlist):

        self.requesting_hotel_sounds_playlist = requesting_hotel_sounds_playlist

        logger.debug(
            "Requesting hotel sounds playlist from hotel: {}.".format(
                self.requesting_hotel_sounds_playlist
            )
        )

    def set_hotel_sounds_playlist(self, hotel_sounds_playlist):

        self.hotel_sounds_playlist = hotel_sounds_playlist

        logger.debug(
            "Setting hotel sounds playlist: {}.".format(self.hotel_sounds_playlist)
        )

    def set_requesting_emergency(self, requesting_emergency):

        self.requesting_emergency = requesting_emergency

        logger.debug(
            "Requesting emergency from hotel: {}.".format(self.requesting_emergency)
        )

    def sync(self):

        self._db.purge()
        self._db.insert(
            {
                "dname": "data",
                "requesting_wifi_password": self.requesting_wifi_password,
                "wifi_password": self.wifi_password,
                "requesting_hotel_sounds_playlist": self.requesting_hotel_sounds_playlist,
                "hotel_sounds_playlist": self.hotel_sounds_playlist,
                "requesting_emergency": self.requesting_emergency,
            }
        )
        self._db.close()

        logger.debug("Synced assistant data.")


__all__ = ["Assistant", "Hotel", "Requester"]
